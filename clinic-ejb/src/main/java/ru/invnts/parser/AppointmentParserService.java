package ru.invnts.parser;

import javax.ejb.Local;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;

@Local
public interface AppointmentParserService {
	public String getStatus();
}
