package ru.invnts.parser;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import ru.invnts.parser.persistence.Clinic;
import ru.invnts.parser.util.AppointmentParser;

/**
 * Session Bean implementation class AppointmentParserServiceBean
 */

@Singleton
@Startup
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AppointmentParserServiceBean implements AppointmentParserService {

	private ParserServiceBean parserBean;
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AppointmentParserServiceBean() {
		// TODO Auto-generated constructor stub
	}

	public EntityManager getEm() {
		return em;
	}

	@EJB
	public void setParserBean(ParserServiceBean parserBean) {
		this.parserBean = parserBean;
	}

	@PersistenceContext(name = "AppointmentParser")
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@PostConstruct
	public void onStartup() {
		System.out.println("Appointment Parser Initialization success.");
	}

	@Override
	public String getStatus() {
		Query q = em.createNativeQuery("SELECT name FROM Clinic LIMIT 1");
		Object str = q.getSingleResult();
		return str.toString();
	}

//	@Schedule(hour = "*/3", persistent = false)
	@Schedule(minute = "*\30", hour = "*", persistent = false)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void parseJob() throws Exception {

		System.out.println("Starting parser..");

		AppointmentParser ap = new AppointmentParser(em);
		ap.parseDistrict();
		em.flush();

		Set<Clinic> clinics = ap.parseClinicsList();
		for (Clinic clinic : clinics) {
			parserBean.parseClinic(ap.getcSRFToken(), clinic);
		}
		System.out.println("Parser stopped!");

	}

}