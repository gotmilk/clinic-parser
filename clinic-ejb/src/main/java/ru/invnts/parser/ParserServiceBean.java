package ru.invnts.parser;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import ru.invnts.parser.persistence.Clinic;
import ru.invnts.parser.util.AppointmentParser;

@Stateless
@LocalBean
public class ParserServiceBean {

	@Resource
	private SessionContext ctx;
	private EntityManager em;

	@PersistenceContext(name = "AppointmentParser")
	public void setEm(EntityManager em) {
		this.em = em;
	}

	public ParserServiceBean() {
	}

	/*
	 * Parse clinic asynchronously & save
	 */
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void parseClinic(String sCRFToken, Clinic clinic) {
		
		AppointmentParser ap = new AppointmentParser(em);
		ap.setcSRFToken(sCRFToken);
		ap.parseClinic(clinic);
		
		System.out.println("Saving " + clinic.getName() + "...");
		try {
			em.merge(clinic);
			em.flush();
			System.out.println("Saved " + clinic.getName() + "!");
		} catch (PersistenceException ex) {
			System.err.println("Persisting error!");
		} catch (Exception ex) {
			System.err.println("Unknown error!");
		}
	}

}
