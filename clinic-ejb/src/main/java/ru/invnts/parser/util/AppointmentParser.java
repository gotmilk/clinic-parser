package ru.invnts.parser.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import ru.invnts.parser.persistence.Clinic;
import ru.invnts.parser.persistence.District;
import ru.invnts.parser.persistence.Doctor;
import ru.invnts.parser.persistence.Speciality;

public class AppointmentParser {

	private CookieManager cookieManager;
	private String cSRFToken;
	private EntityManager em;

	public AppointmentParser(EntityManager em) {
		cookieManager = new CookieManager();

	}

	public String getcSRFToken() {
		return cSRFToken;
	}

	public void setcSRFToken(String cSRFToken) {
		this.cSRFToken = cSRFToken;
		
		HttpCookie httpCookie = new HttpCookie("csrftoken", cSRFToken);
		cookieManager.getCookieStore().add(null, httpCookie);
		
	}

	public String getContent(String urlStr, String method, Map<String, Object> params) {

		String strLine = "";
		String HTMLtext = "";

		if (!(method.equals("GET") || method.equals("POST")))
			return HTMLtext;

		try {
			URL url = new URL(urlStr);

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod(method);
				conn.setRequestProperty("User-Agent", AppointmentURL.USER_AGENT);

				if (cookieManager.getCookieStore().getCookies().size() > 0) {
					String cookies = "";
					for (HttpCookie cookie : cookieManager.getCookieStore().getCookies()) {
						cookies += cookie.toString() + ";";
					}
					conn.setRequestProperty("Cookie", cookies);
				}

				if (method.equals("GET") && params != null) {
					urlStr += "?" + String.valueOf(AppointmentParserUtil.encodeParams(params));
				}

				if (method.equals("POST") && params != null) {
					byte[] postDataBytes = AppointmentParserUtil.encodeParams(params);
					conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
					conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
					conn.setDoOutput(true);
					conn.setConnectTimeout(10000);
					conn.setReadTimeout(10000);
					conn.getOutputStream().write(postDataBytes);
				}

				try (InputStream isr = conn.getInputStream();
						BufferedReader in = new BufferedReader(new InputStreamReader(isr, "UTF-8"))) {

					Map<String, List<String>> headerFields = conn.getHeaderFields();
					
					List<String> cookiesHeader = headerFields.get("Set-Cookie");

					if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
						System.out.println("Unknown response code.." + conn.getResponseCode());
						throw new Exception();
					}

					if (cookiesHeader != null) {
						for (String cookie : cookiesHeader) {
							HttpCookie httpCookie = HttpCookie.parse(cookie).get(0);

							if (httpCookie.getName().equals("csrftoken")) {
								this.cSRFToken = httpCookie.getValue();
							}
							cookieManager.getCookieStore().add(null, httpCookie);
						}
					}

					while ((strLine = in.readLine()) != null) {
						HTMLtext += strLine;
					}

				} catch (IOException e) {
					System.out.println("Error while retrieving content from: " + urlStr);
				} catch (Exception e) {
					System.out.println("Unknown error while retrieving: " + urlStr);
				}

				conn.disconnect();

			} catch (Exception e) {
				System.out.println("Malformed URL provided for " + urlStr);
			}

		} catch (Exception e) {
			System.out.println("Malformed URL provided for " + urlStr);
		}

		return HTMLtext;
	}

	public Set<Clinic> parseClinicsList() {

		String srcHTML;

		Map<String, Object> params = new LinkedHashMap<>();
		params.put("csrfmiddlewaretoken", getcSRFToken());
		params.put("clinic_filter_form-district_id", "");
		params.put("clinic_filter_form-clinic_type", "");
		params.put("clinic_filter_form-clinic_name", "");
		srcHTML = this.getContent(AppointmentURL.CLINIC_REF_URL, "POST", params);
		Set<Clinic> clinics = AppointmentParserUtil.parseClinics(srcHTML);

		return clinics;
	}

	public void parseDistrict() {
		String srcHTML;

		srcHTML = this.getContent(AppointmentURL.INIT_URL, "GET", null);
		Set<District> districts = AppointmentParserUtil.parseDistricts(srcHTML);

		Iterator<District> districtIt = districts.iterator();
		while (districtIt.hasNext()) {
			District district = districtIt.next();
			em.merge(district);
		}
	}

	public void parseClinic(Clinic clinic) {

		Map<String, Object> params = new LinkedHashMap<>();
		String srcHTML;

		params.put("csrfmiddlewaretoken", getcSRFToken());
		params.put("clinic_form-clinic_id", clinic.getId2());
		params.put("clinic_form-patient_id", "");
		params.put("clinic_form-history_id", "");
		srcHTML = this.getContent(AppointmentURL.SPECIALITIES_REF_URL, "POST", params);
		
		Set<Speciality> specialities = AppointmentParserUtil.parseSpecialities(srcHTML);
		Iterator<Speciality> itrSpeciality = specialities.iterator();

		while (itrSpeciality.hasNext()) {
			Speciality speciality = itrSpeciality.next();
			try {
				params = new LinkedHashMap<>();
				params.put("csrfmiddlewaretoken", getcSRFToken());
				params.put("speciality_form-clinic_id", clinic.getId2());
				params.put("speciality_form-speciality_id", speciality.getId());
				params.put("speciality_form-patient_id", "");
				params.put("speciality_form-history_id", "");

//				System.out.println("Clinic: " + clinic.getName() + "// " + speciality.getName());

				srcHTML = this.getContent(AppointmentURL.DOCTORS_REF_URL, "POST", params);
				if (srcHTML != null) {
					Set<Doctor> doctors = AppointmentParserUtil.parseDoctors(srcHTML, clinic, speciality);
					clinic.addDoctors(doctors);
				}
			} catch (Exception e) {
				System.out.println("Unknown exception");
			}
		}
	}

}
