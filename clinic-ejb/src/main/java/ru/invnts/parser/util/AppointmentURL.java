package ru.invnts.parser.util;

public interface AppointmentURL {

	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
	
	public static final String INIT_URL = "http://www.kuban-online.ru/reference/list/";
	public static final String CLINIC_REF_URL = "http://www.kuban-online.ru/api/clinic_reference_list/";
	public static final String SPECIALITIES_REF_URL = "http://www.kuban-online.ru/api/speciality_list/";
	public static final String DOCTORS_REF_URL = "http://www.kuban-online.ru/api/doctor_list/";

}
