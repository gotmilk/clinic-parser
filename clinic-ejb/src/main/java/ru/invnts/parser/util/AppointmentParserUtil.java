package ru.invnts.parser.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.invnts.parser.persistence.Clinic;
import ru.invnts.parser.persistence.District;
import ru.invnts.parser.persistence.Doctor;
import ru.invnts.parser.persistence.Speciality;

/*
 *
 */
public class AppointmentParserUtil {
	
	public static Set<Speciality> parseSpecialities(String html) {
		
		Set<Speciality> result = new HashSet<>();
		
		// create ObjectMapper instance
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			// read JSON like DOM Parser
			JsonNode rootNode = objectMapper.readTree(html);
			JsonNode successNode = rootNode.path("success");
			
			if (successNode.asBoolean()) {
				
				JsonNode arrNode = rootNode.get("response");
				if (arrNode.isArray()) {
				    for (JsonNode objNode : arrNode) {
				    	
				    	Speciality speciality = new Speciality(objNode.get("IdSpesiality").asInt(), objNode.get("NameSpesiality").asText());
				    	result.add(speciality);
				    }
				}
			}
		} catch (JsonMappingException e) {
			System.out.println("JSON Mapping Error");
		} catch (IOException e) {
			System.out.println("JSON I/O Error");
		} catch (Exception e) {
			System.out.println("Unknown JSON Error");
		}

		return result;
	}
	
	public static Set<Doctor> parseDoctors(String html, Clinic clinic, Speciality speciality) {
		Set<Doctor> result = new HashSet<>();
		
		// create ObjectMapper instance
		ObjectMapper objectMapper = new ObjectMapper();
				
		try {
			// read JSON like DOM Parser
			JsonNode rootNode = objectMapper.readTree(html);
			JsonNode successNode = rootNode.path("success");
					
			if (successNode.asBoolean()) {
						
				JsonNode arrNode = rootNode.get("response");
				if (arrNode.isArray()) {
					for (JsonNode objNode : arrNode) {
						int docId = objNode.get("IdDoc").asInt();    	
						Doctor doctor = new Doctor(docId, objNode.get("Name").asText(), clinic);	
							
						String area = objNode.get("AriaNumber").asText();
						
			    		if (area != null && !area.equalsIgnoreCase("null")) {
			    			doctor.addArea(area);
			    		}
						    	
						String snils = objNode.get("Snils").asText();
						
						if (snils!=null && !snils.equalsIgnoreCase("null"))
							doctor.setSnils(snils);

						    	doctor.setTicketsCount(objNode.get("CountFreeTicket").asInt());
						    	doctor.setClinic(clinic);
						    	doctor.setSpeciality(speciality);
						    	doctor.setAvailable(true);
						    	result.add(doctor);
						    }
						}
					}
		} catch (JsonMappingException e) {
			System.out.println("JSON Mapping Error");
		} catch (IOException e) {
			System.out.println("JSON I/O Error");
		} catch (Exception e) {
			System.out.println("Unknown JSON Error");
		}
		
		return result;
	}
	
	public static Set<Clinic> parseClinics(String html) {
		
		Set<Clinic> result = new HashSet<>();
		
		// create ObjectMapper instance
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			// read JSON like DOM Parser
			JsonNode rootNode = objectMapper.readTree(html);
			JsonNode successNode = rootNode.path("success");
			
			if (successNode.asBoolean()) {
				
				JsonNode arrNode = rootNode.get("response");
				if (arrNode.isArray()) {
				    for (JsonNode objNode : arrNode) {
				    	
				    	Clinic clinic = new Clinic(objNode.get("Id").asInt(), objNode.get("LpuName").asText());
				    	clinic.setId2(objNode.get("ExternalHubId").asInt());
				    	clinic.setAddress(objNode.get("Address").asText());
				    	clinic.setPhone(objNode.get("PhoneCallCentre").asText());
				    	clinic.setEmail(objNode.get("email").asText());
				    	clinic.setType(objNode.get("LpuType").asText());
				    	clinic.setComment(objNode.get("Comment").asText());

				    	clinic.setAvailableNet(objNode.get("IsEnableInternet").asBoolean());
				    	clinic.setAvailable(objNode.get("IsEnableAppointment").asBoolean());
				    	
				    	Integer districtId = objNode.get("IdDistrict").asInt();
				    	clinic.setDistrict(new District(districtId));
				    	result.add(clinic);
				    }
				}
			}
		} catch (JsonMappingException e) {
			System.out.println("JSON Mapping Error");
		} catch (IOException e) {
			System.out.println("JSON I/O Error");
		} catch (Exception e) {
			System.out.println("Unknown JSON Error");
		}
		
		return result;
	}
	
	public static Set<District> parseDistricts(String html) {
		
		Set<District> result = new HashSet<>();
		Pattern p = Pattern.compile("clinic_filter_form-district_id\">\\s*(<option value=\"(\\d)*\"(\\s+selected=\"selected\")*>([A-Za-zР-пр-џ\\-,\\s]+)</option>\\s*)+");
		Matcher m = p.matcher(html);
		if (m.find()) {
			html = html.substring(m.start(), m.end());
			
			p = Pattern.compile("<option value=\"(\\d+)\"(\\s+selected=\"selected\")*>([A-Za-zР-пр-џ\\-,\\s]+)</option>\\s*");
			m = p.matcher(html);	
			
			while (m.find()) {
				try {
					int valueId = Integer.valueOf(m.group(1));
					result.add(new District(valueId, m.group(3)));
				} catch (NumberFormatException e) {
					System.out.println("districts parse error");
				}
			}
		}
		
		return result;
	}
	
	public static byte[] encodeParams(Map<String, Object> params) {
		StringBuilder postData = new StringBuilder();
		byte[] postDataBytes = null;
		
		try {
	        for (Map.Entry<String,Object> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        postDataBytes = postData.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
        	System.out.println("encode params error");
        }
		
        return postDataBytes;
	}
}
