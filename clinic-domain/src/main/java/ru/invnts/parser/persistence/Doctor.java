package ru.invnts.parser.persistence;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

@Entity
@NamedQueries({
	@NamedQuery(name="Doctor.FindDoctorByIds",
			query="SELECT d FROM Doctor d WHERE d.id=:id"),
	@NamedQuery(name="Doctor.FindDoctorByName",
			query="SELECT d FROM Doctor d WHERE d.name LIKE '%:name%'")
})
@NamedNativeQuery(name="Doctor.DeleteAreas",
			query="DELETE FROM Area a WHERE a.id = ?1 AND a.clinic = ?2")
public class Doctor implements Comparable<Doctor>, Serializable {

	private String name;
	
	@EmbeddedId
	private DoctorId id;
	
	@MapsId("clinicId")
	@ManyToOne
	@JoinColumn(name="clinic", referencedColumnName="id", nullable=true)
	private Clinic clinic;
	
	@OneToOne
	@JoinColumn(name="DEPARTMENT")
	private Department department;
	
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="SPECIALITY")
	private Speciality speciality;
	
	
	@OneToMany(mappedBy="doctor")
	private Set<Ticket> tickets = new HashSet<>();
	
	
	private String snils;
	private boolean available;
	
	private int ticketsCount;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(
			name="AREA",
			joinColumns={
					@JoinColumn(name="id", referencedColumnName="id"),
					@JoinColumn(name="clinic", referencedColumnName="clinic")
			}
	)
	@Column(name="name")
	@OrderBy
	private Set<String> areas = new HashSet<>();

	public String getSnils() {
		return snils;
	}

	public void setSnils(String snils) {
		this.snils = snils;
	}

	public int getTicketsCount() {
		return ticketsCount;
	}

	public void setTicketsCount(int ticketsCount) {
		this.ticketsCount = ticketsCount;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Doctor() {}

	public Doctor(int id, String name, Clinic clinic) {
//		this.id = id;
		this.clinic = clinic;
		this.id = new DoctorId(id, clinic.getId());
		this.name = name;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}
	
	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public int getId() {
		return id.getId();
	}

	public String getName() {
		return name;
	}

	
	public Set<Ticket> getTickets() {
		return tickets;
	}
	
	public int compareTo(Doctor doctor) {
		if (this.id.getId() != doctor.id.getId()) {
			int val_id = this.name.compareTo(doctor.name);
			if (val_id == 0) {
				val_id = this.id.getId() - doctor.id.getId();
			}
			return val_id;
		} 
		
		return 0;
	}

	public Set<String> getAreas() {
		return areas;
	}
	
	public void addArea(String areaName) {
		this.getAreas().add(areaName);
	}
	
	public void clearArea() {
		this.getAreas().clear();
	}

	public void setAreas(Set<String> areas) {
		this.areas = areas;
	}
	
}
