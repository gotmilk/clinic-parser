package ru.invnts.parser.persistence;

import java.io.*;
import javax.persistence.*;

@Entity
@NamedQueries({
	@NamedQuery(name = "District.SelectAllDistricts",
			query="SELECT DISTINCT(d) FROM District d ORDER BY d.id ASC")
})
public class District implements Comparable<District>, Serializable {

	@Id
	private int id;
	
	private String name;

	
	public District() { }
	
	public District(int id) {
		this.id = id;
	}
	
	public District(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public int compareTo(District district) {
		return Integer.compare(id, district.getId());
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof District) {
			return Integer.compare(((District)obj).getId(), id)==0?true:false;
		}
			
		return false;

	}
	
	
	
}
