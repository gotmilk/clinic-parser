package ru.invnts.parser.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Ticket implements Comparable<Ticket>, Serializable {
	
	@Id
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date endDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date date;
	
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="doctor", referencedColumnName="id"),
		@JoinColumn(name="clinic", referencedColumnName="clinic")
	})
	private Doctor doctor;
	
	
	private String cabinet;
	private boolean available;
	
	public Ticket() {}

	public Ticket(int id, Date date, Doctor doctor) {
		this.id = id;
		this.date = date;
		this.doctor = doctor;
	}

	public java.util.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public java.util.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	public String getCabinet() {
		return cabinet;
	}

	public void setCabinet(String cabinet) {
		this.cabinet = cabinet;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getId() {
		return id;
	}

	public java.util.Date getDate() {
		return date;
	}

	
	public Doctor getDoctor() {
		return doctor;
	}
	

	@Override
	public int compareTo(Ticket ticket) {
		if (this.doctor.getId() != ticket.id) return this.date.compareTo(ticket.date);
		return Integer.compare(this.getId(), ticket.getId());
	}
	
	
	
}
