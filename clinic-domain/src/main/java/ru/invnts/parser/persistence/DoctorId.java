package ru.invnts.parser.persistence;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Access(AccessType.FIELD)
public class DoctorId implements Serializable {

	private int id;

	@Column(name="clinic", insertable=false, updatable=false, nullable=true)
	private int clinicId;
	
	public DoctorId() {}
	
	public DoctorId(int id, int clinicId) {
		this.id = id;
		this.clinicId = clinicId;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public boolean equals(Object o) {
		return ((o instanceof DoctorId) 
				&& (((DoctorId)o).getId() == this.id)
				&& ((DoctorId)o).getClinicId() == this.clinicId);
	}

	public int getId() {
		return id;
	}

	public int getClinicId() {
		return this.clinicId;
	}
	
	
	
	
}
