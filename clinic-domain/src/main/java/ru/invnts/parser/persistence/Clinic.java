package ru.invnts.parser.persistence;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

@Entity
@NamedQueries({
	@NamedQuery(name="Clinic.FindClinicByName", 
			query="SELECT c FROM Clinic c WHERE c.name LIKE :name"),
	@NamedQuery(name="Clinic.SelectAllClinics",
			query="SELECT c FROM Clinic c ORDER BY c.id ASC"),
})
public class Clinic implements Serializable, Comparable<Clinic> {
	
	@Id
	private int id;
	
	@OneToMany(mappedBy="clinic", cascade={CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
	@OrderBy
	private Set<Doctor> doctors = new HashSet<>();
	
	private int id2;
	private String name;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="district")
	private District district;
	
	private String address;
	private String phone;
	private String email;
	private boolean available;
	private boolean availableNet;
	private String type;
	
	private String comment;
	
	public Clinic () {}
	public Clinic (int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	
	/*
	public Integer getTmpDistrictId() {
		return tmpDistrictId;
	}
	public void setTmpDistrictId(Integer tmpDistrictId) {
		this.tmpDistrictId = tmpDistrictId;
	}
	*/
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public boolean isAvailableNet() {
		return availableNet;
	}
	public void setAvailableNet(boolean availableNet) {
		this.availableNet = availableNet;
	}
	
	public void addDoctors(Set<Doctor> doctors) {
		this.doctors.addAll(doctors);
	}
	
	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}
	
	public Set<Doctor> getDoctors() {
		return doctors;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public int compareTo(Clinic clinic) {
		return Integer.compare(id, clinic.getId());
	}
	@Override
	public int hashCode() {
		return this.id;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Clinic) {
			return Integer.compare(id, ((Clinic)obj).getId())==0?true:false;
		}
		
		return false;
	}
	public int getId2() {
		return id2;
	}
	public void setId2(int id2) {
		this.id2 = id2;
	}
	
	
	
}
