package ru.invnts.parser.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Speciality implements Serializable {

	@Id
	private int id;
	private String name;
	
	public Speciality() {}
	
	public Speciality(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	
	
}
