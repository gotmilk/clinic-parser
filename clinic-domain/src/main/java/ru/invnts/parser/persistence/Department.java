package ru.invnts.parser.persistence;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Department implements Serializable {
	
	@Id
	private int id;
	private String name;
	
	public Department() {}

	public Department(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	 
	
}
